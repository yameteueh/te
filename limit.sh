#!/bin/bash

NUM_CPU_CORES=$(nproc --all)

cpulimit -e "kelopo" -l $((50 * $NUM_CPU_CORES))